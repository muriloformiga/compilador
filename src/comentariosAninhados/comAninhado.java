package comentariosAninhados;

import java.io.IOException;
import java.io.PushbackReader;
import java.util.Stack;

import especific.lexer.* ;
import especific.node.* ; 

public class comAninhado extends Lexer {
	
	private TComentarioBloco comment;
	private Stack<int []> pilha;

	public comAninhado(PushbackReader pushbackReader) {
		super(pushbackReader);
		// TODO Auto-generated constructor stub
	}
	
	protected void filter() throws LexerException, IOException {
		if(state.equals(State.COMMENT)){ 
			if (pilha == null) {
				if (token instanceof TComentarioblocoFim) {
					throw new LexerException (null, 
							"["+token.getLine()+", "+token.getPos()+"] "
							+ "Erro de Comentario de Bloco: "+token.getClass().getSimpleName());
				}
				pilha = new Stack<int []>();
				int pos[] = {token.getLine(), token.getPos()};
				pilha.push(pos);
				comment = (TComentarioBloco) token;
		        token = null;
			} else {
				if (token instanceof TComentarioBloco) {
					int pos[] = {token.getLine(), token.getPos()};
					pilha.push(pos);
				} else if (token instanceof TComentarioblocoFim) {
					pilha.pop();
				} else if (token instanceof EOF && !pilha.empty()) {
					int pos[] = pilha.pop();
					throw new LexerException (null, 
							"["+pos[0]+", "+pos[1]+"] Erro de Comentario de Bloco: TComentarioBloco");
				}
				
				// Comfirma se acabou o bloco de cometário  
				if (pilha.empty()) {
		        	token = comment; //return a comment with the full text.
		        	state = State.NORMAL; //go back to normal.
			        pilha = null;
				} else {
					token = null;
				}
			}
		}
		
    }

}
