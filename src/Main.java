import especific.node.* ; 
import java.io.* ;
import comentariosAninhados.comAninhado; 
  
public class Main { 
	public static void main(String[] args)
	{
		try
		{
			String arquivo = "tester.sa";

			comAninhado lexer =
					new comAninhado(
							new PushbackReader(  
									new FileReader(arquivo), 1024)); 
			Token token;

			while(!((token = lexer.next()) instanceof EOF)) {
				if (token.getClass().getSimpleName().equals("TVazio")) {
					System.out.print(token);
				} else {
					System.out.print(token.getClass().getSimpleName());
				}
			}
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
		}
	}
}